#solving the fractional knapsack problem using greedy method

def knapsack_gdy(value, weight, capacity):
    index = list(range(len(value)))                                                 #[0, 1, 2, 3]
    ratio = [v/w for v, w in zip(value, weight)]                                    #[6.0, 1.0, 5.0, 4.0]
    index.sort(key=lambda i : ratio[i], reverse = True)                             #[0->6.0, 2->5.0, 3->4.0, 1->1.0]

    max_value = 0 
    fractions = [0] * len(value)
    selected = []
    for i in index:
        if weight[i] <= capacity:
            fractions[i] = 1
            max_value += value[i]
            capacity -= weight[i]
            selected.append(str(value[i]))
        else:
            fractions[i] = capacity/weight[i]
            max_value += value[i]*capacity/weight[i]
            selected.append(str(round(fractions[i], 2)) + " * " + str(value[i]))
            break
    return [selected, max_value]


    