#0/1 knapsack problem solution using brute force.
def get_strings(n):
    return [ bin(x)[2:].rjust(n, "0") for x in range (2**n)]

def knapsack(capacity, weight, value):
    n = len(weight)    
    solutions = get_strings(n)    
    profits = [0] * len(solutions)
    for i in solutions:
        curr_wt = 0
        for j in range(0,len(i)):
            if(i[j]=="1" and curr_wt + weight[j] <= capacity):
                profits[int(i, 2)] +=value[j]
                curr_wt += weight[j]

    selected = solutions[profits.index(max(profits))]
    values = []
    for x in range(0, len(selected)):
        if selected[x] == "1":
            values.append(value[x])

    return [values, max(profits)]


