import unittest
from knapsack_bf import knapsack
from fractional_bf import knapsack_frac
from knapsack_gdy import knapsack_gdy
from knapsack_dp import knapSack

class TestKnapsack(unittest.TestCase):

    def test_knapsack01_bruteForce(self):
        value =  [1, 20, 3, 14, 100]
        weight = [2, 10, 3, 6, 18]
        capacity = 15
        n = len(value)
        self.assertListEqual(knapsack(capacity, weight, value),[[1, 20, 3], 24])

    def test_knapsackFrac_bruteForce(self):
        capacity = 50
        weight = [10, 40, 20 , 30]
        value = [60, 40, 100, 120]
        self.assertListEqual(knapsack_frac(capacity, weight, value), [["60", "100", "0.67 * 120"], 240.0])

    def test_knapsackFrac_greedy(self):
        wt = [10, 40, 20, 30] 
        val = [60, 40, 100, 120] 
        capacity = 50
        self.assertListEqual(knapsack_gdy(val, wt, capacity), [['60', '100', '0.67 * 120'], 240.0])


    def test_knapsack_dp(self):
        item_wt = [2, 10, 3, 6, 18]
        item_val = [1, 20, 3, 14, 100]
        W = 15
        n = len(item_val) 
        self.assertListEqual(knapSack(W, item_wt, item_val, n), [[3, 20, 1], 24])


if __name__=="__main__":
    unittest.main()
