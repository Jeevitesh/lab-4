
def knapSack(W, wt, val, n): 
	mat = [[0 for x in range(W + 1)] for x in range(n + 1)] 

	for i in range(n + 1): 
		for w in range(W + 1): 
			if i == 0 or w == 0: 
				mat[i][w] = 0
			elif wt[i-1] <= w: 
				mat[i][w] = max(val[i-1] + mat[i-1][w-wt[i-1]], mat[i-1][w]) 
			else: 
				mat[i][w] = mat[i-1][w] 

	result = mat[n][W]
	w = W 
	selected = []

	for i in range(n, 0, -1):
		if result <= 0:
			break
		if result == mat[i-1][w]:
			continue
		else:
			selected.append(val[i - 1])
			result = result - val[i - 1]
			w = w - wt[i - 1] 
		
	return [selected, mat[n][W]]


	



