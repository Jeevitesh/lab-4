def get_strings(n):
   return [list(bin(x)[2:].rjust(n, '0')) for x in range(2**n)]

def knapsack_frac(capacity, weight, value):
    n = len(weight)
    maxVal = 0
    selections = ['0'] * n     
    solutions = get_strings(n)
    
    for s in solutions:
        
        item_rem = [i for i, x in enumerate(s) if x == '0']        
        val = sum([int(s[i]) * value[i] for i in range(n)])
        wt = sum([int(s[i]) * weight[i] for i in range(n)])
        
        fracVal = 0
        temp_solution = s

        if wt<capacity:
            maxIndex = 0
            for i in item_rem:
                rem = capacity-wt if(capacity-wt < weight[i]) else weight[i]
                frac = (value[i]/weight[i])*(rem)
                if frac > fracVal:
                    fracVal = frac
                    maxIndex = i
            temp_solution[maxIndex] = str(round(fracVal/value[i],2)) + " * " + str(value[i])
                
        totalVal = fracVal + val
    
        if wt<= capacity and totalVal >= maxVal:
            maxVal = totalVal
            selections = temp_solution
    
    selected = []

    for x in range(0, len(selections)):
        if selections[x] == "1":
            selected.append(str(value[x]))
        elif (selections[x] != "0"):
            selected.append(selections[x])

    return [selected,maxVal]


